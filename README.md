# LaTeX-Tools

A collection of macros for LaTeX at TEMF.

## Creation of the .sty-file
Open a console and navigate to the folder where the .dtx and the .ins file are saved. Then run:

``latex temfmacros.ins``

## Creation of the documentation
Open a console and navigate to the folder where the .dtx and the .ins file are saved. Then run:

``pdflatex temfmacros.dtx``

You may run this command several times to make sure that all cross references inside the documentation are working. 

## Where to keep the files
Create a new TEXMF root, for example in your home directory. Therefor, chreate the following folder strucutre:

``mytexmf/tex/latex`` 

Clone this repository in the just created ``latex`` folder and create the .sty-file. Finally, register the TEXMF root directroy ``mytexmf`` . The last step can be carried out in the MikTeX console. Then the package can be found from anywhere on your computer. 

